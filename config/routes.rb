Hitchik::Application.routes.draw do
  #get "users/get"

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  resources :users, only: [:show] do
    member do
      post :rate
    end
  end

  resources :hitchikes do
    resources :reservations, only: [:create, :destroy, :new]
    get :my, :on => :collection
    get :search, :on => :collection
    get :wtf, :on => :collection
    #Rating
    member do
      post :rate
    end
  end

  resources :hitchike_requests

  #get "static_pages/home"
  #get "static_pages/terms"
  #get "static_pages/about"

  match '/terms',   to: 'static_pages#terms'
  match '/contact', to: 'static_pages#contact'
  match '/about', to: 'static_pages#about'

  # para meter o devise so com facebook
  #devise_scope :user do
  #  get 'sign_in', :to => 'devise/sessions#new', :as => :new_user_session
  #  delete 'sign_out', :to => 'devise/sessions#destroy', :as => :destroy_user_session
  #end

  root :to => 'static_pages#home'




  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
#== Route Map
# Generated on 03 Jan 2013 15:44
#
#         new_user_session GET    /users/sign_in(.:format)                           devise/sessions#new
#             user_session POST   /users/sign_in(.:format)                           devise/sessions#create
#     destroy_user_session DELETE /users/sign_out(.:format)                          devise/sessions#destroy
#  user_omniauth_authorize        /users/auth/:provider(.:format)                    users/omniauth_callbacks#passthru {:provider=>/facebook/}
#   user_omniauth_callback        /users/auth/:action/callback(.:format)             users/omniauth_callbacks#(?-mix:facebook)
# cancel_user_registration GET    /users/cancel(.:format)                            devise/registrations#cancel
#        user_registration POST   /users(.:format)                                   devise/registrations#create
#    new_user_registration GET    /users/sign_up(.:format)                           devise/registrations#new
#   edit_user_registration GET    /users/edit(.:format)                              devise/registrations#edit
#                          PUT    /users(.:format)                                   devise/registrations#update
#                          DELETE /users(.:format)                                   devise/registrations#destroy
#    hitchike_reservations POST   /hitchikes/:hitchike_id/reservations(.:format)     reservations#create
# new_hitchike_reservation GET    /hitchikes/:hitchike_id/reservations/new(.:format) reservations#new
#     hitchike_reservation DELETE /hitchikes/:hitchike_id/reservations/:id(.:format) reservations#destroy
#             my_hitchikes GET    /hitchikes/my(.:format)                            hitchikes#my
#         search_hitchikes GET    /hitchikes/search(.:format)                        hitchikes#search
#            wtf_hitchikes GET    /hitchikes/wtf(.:format)                           hitchikes#wtf
#            rate_hitchike POST   /hitchikes/:id/rate(.:format)                      hitchikes#rate
#                hitchikes GET    /hitchikes(.:format)                               hitchikes#index
#                          POST   /hitchikes(.:format)                               hitchikes#create
#             new_hitchike GET    /hitchikes/new(.:format)                           hitchikes#new
#            edit_hitchike GET    /hitchikes/:id/edit(.:format)                      hitchikes#edit
#                 hitchike GET    /hitchikes/:id(.:format)                           hitchikes#show
#                          PUT    /hitchikes/:id(.:format)                           hitchikes#update
#                          DELETE /hitchikes/:id(.:format)                           hitchikes#destroy
#        hitchike_requests GET    /hitchike_requests(.:format)                       hitchike_requests#index
#                          POST   /hitchike_requests(.:format)                       hitchike_requests#create
#     new_hitchike_request GET    /hitchike_requests/new(.:format)                   hitchike_requests#new
#    edit_hitchike_request GET    /hitchike_requests/:id/edit(.:format)              hitchike_requests#edit
#         hitchike_request GET    /hitchike_requests/:id(.:format)                   hitchike_requests#show
#                          PUT    /hitchike_requests/:id(.:format)                   hitchike_requests#update
#                          DELETE /hitchike_requests/:id(.:format)                   hitchike_requests#destroy
#                    terms        /terms(.:format)                                   static_pages#terms
#                  contact        /contact(.:format)                                 static_pages#contact
#                    about        /about(.:format)                                   static_pages#about
#                     root        /                                                  static_pages#home
