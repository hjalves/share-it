class AddSeatsToHitchikes < ActiveRecord::Migration
  def change
    add_column :hitchikes, :seats, :integer
  end
end
