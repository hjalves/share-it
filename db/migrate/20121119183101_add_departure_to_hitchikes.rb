class AddDepartureToHitchikes < ActiveRecord::Migration
  def change
    add_column :hitchikes, :departure, :datetime
  end
end
