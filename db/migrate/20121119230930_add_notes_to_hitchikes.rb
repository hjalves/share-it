class AddNotesToHitchikes < ActiveRecord::Migration
  def change
    add_column :hitchikes, :notes, :text
  end
end
