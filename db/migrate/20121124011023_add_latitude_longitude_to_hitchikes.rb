class AddLatitudeLongitudeToHitchikes < ActiveRecord::Migration
  def change
    add_column :hitchikes, :latitude, :float
    add_column :hitchikes, :longitude, :float
  end
end
