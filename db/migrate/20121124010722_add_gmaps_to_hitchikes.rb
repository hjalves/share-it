class AddGmapsToHitchikes < ActiveRecord::Migration
  def change
    add_column :hitchikes, :gmaps, :boolean
  end
end
