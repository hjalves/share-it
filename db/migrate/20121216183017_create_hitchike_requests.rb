class CreateHitchikeRequests < ActiveRecord::Migration
  def change
    create_table :hitchike_requests do |t|
      t.string :source
      t.string :destination
      t.datetime :departure
      t.text :notes
      t.references :user
      
      t.timestamps
    end
  end
end
