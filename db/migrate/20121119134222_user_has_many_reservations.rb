class UserHasManyReservations < ActiveRecord::Migration
  def up
    change_table :reservations do |t|
      t.references :user
    end
  end

  def down
  end
end
