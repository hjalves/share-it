class HitchikeHasManyReservations < ActiveRecord::Migration
  def up
    change_table :reservations do |t|
      t.references :hitchike
    end
  end

  def down
  end
end
