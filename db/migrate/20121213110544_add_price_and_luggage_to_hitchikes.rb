class AddPriceAndLuggageToHitchikes < ActiveRecord::Migration
  def change
    add_column :hitchikes, :price, :decimal, :precision => 8, :scale => 2
    add_column :hitchikes, :luggage, :string
  end
end
