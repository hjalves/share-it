class CreateHitchikes < ActiveRecord::Migration
  def change
    create_table :hitchikes do |t|
      t.string :source
      t.string :destination

      t.timestamps
    end
  end
end
