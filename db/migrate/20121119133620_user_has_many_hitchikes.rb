class UserHasManyHitchikes < ActiveRecord::Migration
  def up
    change_table :hitchikes do |t|
      t.references :user
    end
  end

  def down
  end
end
