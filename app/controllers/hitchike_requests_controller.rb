class HitchikeRequestsController < ApplicationController

    before_filter :authenticate_user!, :except => [:index, :show]

    def index
        @hitchike_requests = HitchikeRequest.all
    end

    def show
    end
    
    def new
        @hitchike_request = current_user.hitchike_requests.new(:source => params[:p],
            :destination => params[:q], :departure => params[:dt])
    end

    def create
        @hitchike_request = current_user.hitchike_requests.new( params[:hitchike_request] )
        @hitchike_request.save
        redirect_to root_path
    end
end
