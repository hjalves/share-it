class HitchikesController < ApplicationController

    before_filter :authenticate_user!, :except => [:index, :show, :search]

    # GET /hitchikes/search
    # TODO: partida e data
    def search
        @hitchikes = Hitchike.select { |h| ! h.is_old }
  
        # filtra partida
        unless params[:p].blank?
            @hitchikes = @hitchikes.select { |h| h.source.casecmp(params[:p].strip) == 0 }
        end
        
        # filtra destino
        
        @circles = { }
        unless params[:q].blank?
            begin
                loc = Gmaps4rails.geocode(params[:q]).first
            rescue
                redirect_to root_path
                return
            end
            raio = params[:d].to_f
            @hitchikes = @hitchikes.select do |h|
                dist = Geocoder::Calculations.distance_between([loc[:lat], loc[:lng]], [h.latitude, h.longitude], :units=>:km)
                dist <= raio
            end
            lat = loc[:lat]
            lng = loc[:lng]
            rad = raio*1000
            @circles = {"circles"    => { "data" => "[ {\"lng\": #{lng}, \"lat\": #{lat}, \"radius\": #{rad}} ]" }}
        end
        
        if params[:ord] == 'd'
            @hitchikes.sort_by!(&:departure)
        end

        @json = @hitchikes.to_gmaps4rails
        @markers = @json

        
        # meter o centro do circulo
        #if params[:q] and params[:q].size > 0
        #    @markers += ".concat( [ {\"lat\": #{lat}, \"lng\": #{lng}} ] )"
        #end

        # filtra data
        unless params[:dt].blank?
            search_date = Date.parse(params[:dt])
            DateTime.now.to_date
            @hitchikes = @hitchikes.select { |h| h.departure.to_date == search_date }
        end

        if @hitchikes.blank?
            @noresults = true
            @hitchike_request = HitchikeRequest.new(:source => params[:p],
                :destination => params[:q], :departure => params[:dt])
            render 'hitchike_requests/new'
            return
        end

        respond_to do |format|
            format.html { render :index }
            format.json { render json: @hitchikes}
        end
    end

    # GET /hitchikes/my
    def my
        @hitchikes = current_user.hitchikes.all      
    end

    # GET /hitchikes
    def index
        @hitchikes = Hitchike.all
        @json = @hitchikes.to_gmaps4rails
        @markers = @json
        @circles = { }

        respond_to do |format|
            format.html { render :index }
            format.json { render json: @hitchikes}
        end

    end

    # GET /hitchikes/1 Implemetado o Maps
    def show
        @hitchike = Hitchike.find( params[:id] )
        @json = @hitchike.to_gmaps4rails

        respond_to do |format|
            format.html # index.html.erb
            format.json { render json: @hitchike}
        end
    end

    # GET /hitchikes/new
    def new
        @hitchike = current_user.hitchikes.new( :departure => DateTime.now.to_s(:short) )
    end

    #  POST /hitchikes/
    def create
        @hitchike = current_user.hitchikes.new( params[:hitchike] )
        if @hitchike.save
            redirect_to @hitchike
        else
            render 'static_pages/home'
        end
    end
    
    #Ranking
    def rate
        @hitchike = Hitchike.find(params[:id])
        @hitchike.rate(params[:stars], current_user, params[:dimension])

    end

    # GET wtf
    def wtf
        @hide = true
        render :noresults
    end
end

