class ReservationsController < ApplicationController

    before_filter :authenticate_user!

    # GET /hitchike/1/reservations/new
    def new
        @hitchike = Hitchike.find( params[:hitchike_id] )
        @reservation = current_user.reservations.new
        @reservation.hitchike = @hitchike
    end

    # POST /hitchike/1/reservations
    def create
        @hitchike = Hitchike.find( params[:hitchike_id] )
        # alternativamente usar new(params[:reservation])
        #TODO: espetar erro ao user

        if @hitchike.user != current_user && @hitchike.free_seats > 0 &&
            ! current_user.reserved_hitchike?(@hitchike) && ! @hitchike.is_old then
                @reservation = current_user.reservations.new( hitchike_id: params[:hitchike_id] )
                @reservation.save
        end
        # redirect_to hitchikes_path
        redirect_to @hitchike
    end

    #DELETE /hitchike/1/reservations/2
    def destroy
        @hitchike = Hitchike.find( params[:hitchike_id] )
        @reservation = current_user.reservations.find( params[:id] )
        @reservation.destroy()
        redirect_to @hitchike
    end
end
