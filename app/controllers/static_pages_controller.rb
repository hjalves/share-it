class StaticPagesController < ApplicationController
    def home
        params[:d] = 10
        params[:p] = "Coimbra"
        params[:q] = "Lisboa"
        @hitchike = Hitchike.new( :price => 0.00, :seats => 4, :departure => DateTime.now.to_s(:short) )
    end

    def about
    end

    def terms
    end

    def contacts
    end
end
