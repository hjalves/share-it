# == Schema Information
#
# Table name: hitchike_requests
#
#  id          :integer          not null, primary key
#  source      :string(255)
#  destination :string(255)
#  departure   :datetime
#  notes       :text
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class HitchikeRequest < ActiveRecord::Base
  attr_accessible :departure, :destination, :notes, :source

  def is_old
    DateTime::now > departure
  end

  belongs_to :user
end
