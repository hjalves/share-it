# == Schema Information
#
# Table name: reservations
#
#  id          :integer          not null, primary key
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  hitchike_id :integer
#

class Reservation < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_accessible :hitchike_id

  belongs_to :user
  belongs_to :hitchike

end
