# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  provider               :string(255)
#  uid                    :string(255)
#  name                   :string(255)
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable, :omniauthable
  
  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :provider, :uid
  # attr_accessible :title, :body

  #Rating
  ajaxful_rateable :stars=> 5, :dimensions => [:avaliacaogeral], :allow_update => true, :cache_colum => :rating_average
  ajaxful_rater

  has_many :hitchikes
  has_many :reservations
  has_many :hitchike_requests

  def profile_image()
    if uid.blank?
      return "https://graph.facebook.com/joao.brito.7549185/picture"
    else
      return "https://graph.facebook.com/#{uid}/picture"
    end
  end
  
  def fb_page()
    if uid.blank?
      return ""
    else
      return "https://facebook.com/#{uid}"
    end
  end

  def reserved_hitchike?(hitchike)
    reservations.any? { |r| r.hitchike == hitchike }
  end

  def find_reservation(hitchike)
    reservations.find { |r| r.hitchike == hitchike }
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    unless user
      puts auth.info.name
      user = User.create(  name: auth.info.name,
                           provider:auth.provider,
                           uid:auth.uid,
                           email:auth.info.email,
                           password:Devise.friendly_token[0,20]
                        )
    end
    user
  end

end
