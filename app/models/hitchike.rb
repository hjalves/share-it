# == Schema Information
#
# Table name: hitchikes
#
#  id             :integer          not null, primary key
#  source         :string(255)
#  destination    :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :integer
#  departure      :datetime
#  seats          :integer
#  notes          :text
#  gmaps          :boolean
#  latitude       :float
#  longitude      :float
#  rating_average :decimal(6, 2)    default(0.0)
#  price          :decimal(8, 2)
#  luggage        :string(255)
#

class Hitchike < ActiveRecord::Base
  attr_accessible :destination, :gmaps, :latitude, :longitude, :source, :departure, :seats, :notes, :rating_average, :price, :luggage
  #Rating
  ajaxful_rateable :stars=> 5, :dimensions => [:avaliacaogeral], :allow_update => true, :cache_colum => :rating_average
  #Maps
  acts_as_gmappable

  def free_seats
    return seats - reservations.length()
  end

  def is_old
    DateTime::now > departure
  end

  def gmaps4rails_address
    destination
  end


  belongs_to :user
  has_many :reservations
end
