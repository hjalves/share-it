# == Schema Information
#
# Table name: hitchikes
#
#  id             :integer          not null, primary key
#  source         :string(255)
#  destination    :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :integer
#  departure      :datetime
#  seats          :integer
#  notes          :text
#  gmaps          :boolean
#  latitude       :float
#  longitude      :float
#  rating_average :decimal(6, 2)    default(0.0)
#  price          :decimal(8, 2)
#  luggage        :string(255)
#

require 'test_helper'

class HitchikeTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
