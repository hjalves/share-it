Share-It Dev Guide
==================

Colaboração
-----------

*TODO:*

*git+bitbucket*


Correr o servidor e Testar
--------------------------

*TODO:*

*rails s, bundle install, rake db:migrate, localhost:3000, criar utilizadores*


Tipos de ficheiros
------------------

Existem vários tipos de ficheiros:

* `.rb` - Código Ruby

* `.html.erb` - Páginas HTML com Ruby

* `.css.scss` - Linguagem SASS (É igual ao CSS mas mais potente - nao estamos a usar nada disso no nosso projeto)


Diretorias
----------


* `app` - pasta onde está praticamente tudo o que é interessante editar

* `app/assets/images` - onde são guardadas as imagens

* `app/assets/stylesheets` - onde são guardados os ficheiros CSS

* `app/assets/javascripts` - onde deviam ser guardados os ficheiros javascript, mas temos tudo espalhado, por enquanto

* `app/controllers/` -  controladores. É onde está a maior parte da lógica do site (a parte mais de programação).

* `app/helpers/` - é para organizar grandes quantidades de código, que em vez de irem para o `.html.erb`, vão para aqui sobre a forma de funções.
não é útil por enquanto.

* `app/models/` - é o modelo de dados, e o que define as "classes" da aplicação. Boleias, Utilizadores, Reservas...

* `app/views/` - aqui está tudo o que se *vê*. As páginas web.

* `config/` - diversos ficheiros de configuração


Sobre as views
--------------

As **views** é todo o resultado final que é visualizável pelo browser. Isto inclui todo o HTML gerado
com CSS e Javascript incluído.

Como o nosso projeto funciona à base de páginas dinâmicas com informação que está constantemente a mudar,
é preciso utilizar o poder de uma linguagem de programação em conjunto com o HTML.

Como é que isto funciona?

Em Rails tudo o que estiver em ficheiros `*.html.erb` e tiver umas tags especiais,
na altura do carregamento da página essas tags são interpretados como Ruby
e o resultado é juntado à página HTML.

As tags especiais são apenas estas duas: `<% %>`, `<%= %>` e mais outras nao importantes especificadas em:
[http://api.rubyonrails.org/classes/ActionView/Base.html](http://api.rubyonrails.org/classes/ActionView/Base.html)

`<%....%>` significa que tudo o que estiver entre os dois sinais de percentagem deve ser interpretado como Ruby.

`<%=...%>` é igual à de cima, com a única diferença que manda o output para a página (como um `print` em MATLAB ou Python)

Além disso o código HTML pode ser usado no meio de construções de Ruby (como ciclos). Um exemplo:

```erb
<% for i in 1..5 %>
   <%="Ola" %>
<% end %>
```

Isto é um dos ciclos em ruby (ciclo for).
Quando a página é carregada, vai aparecer `Ola Ola Ola Ola Ola` em vez desse bloco (por causa da tag `%=` e a string `"Ola"`)

No entanto, isto já não é valido:

```erb
<% for i in 1..5 %>
   <%= Ola %>
<% end %>
```

`Ola` é interpretado como código ruby e ia dar erro, por Ruby não saber o que raio é `Ola`.

Já o seguinte é válido e equivalente ao primeiro:

```erb
<% for i in 1..5 %>
   Ola
<% end %>
```

que pode levar tags html como desejarmos!

```erb
<% for i in 1..5 %>
   <b>Ola </b>
<% end %>
```


No nosso caso utilizamos isto para inquirir sobre os objetos presentes na aplicação, e mostrar o HTML correspondente.
Excerto de `show.html.erb`:

```erb
<div class="texto">
  <p> <b> Partida: </b> <%= @hitchike.source %> </p>
  <p> <b> Destino: </b> <%= @hitchike.destination %> </p>
  <p> <b> Data: </b> <%= @hitchike.departure.to_s(:short) %> </p>
  <p> <b> Lugares totais: </b> <%= @hitchike.seats %> </p>
  <p> <b> Lugares livres: </b> <%= @hitchike.free_seats %> </p>
  <p> <b> Preco: </b> N/A </p>
  <p> <b> Bagagem: </b> N/A </p>
</div>
```

Aqui está um pedaço de html, com Ruby embebido.
`@hitchike` é uma variável (ou um **objeto** - termos de programação orientada a objetos)

`.source`, `.destination`, `.departure` são exemplos de *atributos* do objeto do tipo *Hitchike*

O que é o tipo *Hitchike*?

É um tipo de variável, uma estrutura de dados definida por nós
(em termos de P.O.O. chama-se **classe**) e que tem toda a informação interessante que diz respeito a uma determinada boleia.

Porque é que `@hitchike` tem um **@** atrás?

Bem, isso é um pouco mais complicado de explicar, mas  de uma forma simplista,
todas as variáveis que têm **@** atrás e que se utilizam nas views (ficheiros html.erb!)
vêm do controlador, ou seja, da parte da aplicação que especifica que página vai carregar,
o que vai ser movimentado da base de dados, e toda a lógica da aplicação.

Neste caso `@hitchike` representa a boleia que foi escolhida pelo utilizador (numa página anterior).

Existem outras coisas possivelmente estranhas ali como `to_s(:short)`. Isso é um método
(uma função que diz respeito a um objeto e chama-se da mesma maneira que os atributos).

Neste caso em concreto, `to_s` já vem do Ruby e significa *to string* e é uma forma relativamente universal
de converter qualquer tipo de dados em strings, visto que `departure` é um tempo (data + hora) e
pode ser formatado de diversas formas
(12 Dez 12:30; Quarta, dia 12 de Dezembro às 12h30; 2012-12-12 12:30:00, ...)

É isso que faz o `:short` a seguir a `to_s`. É um parâmetro do método `to_s` que especifica o formato do tempo.

`:short` é um identificador. É uma coisa especial de Ruby e é algo semelhante
a uma string, mas enquanto uma string representa texto (que é suposto ser lido por pessoas),
um identificador é algo para ser utilizado somente em código.

Raramente é preciso usar isto, só mesmo quando as funções de Ruby ou Rails pedem.
É uma coisa que se vê na documentação ou pesquisando no Google.

*TODO: Mais sobre os helpers de Ruby e pagina de documentação*


A organização das views
-----------------------

De momento a organização é a seguinte:

* `/views/hitchikes/my.html.erb` [/hitchikes/my](http://share-it.herokuapp.com/hitchikes/my) -
Página que mostra as próprias boleias

* `/views/hitchikes/index.html.erb` [/hitchikes/search](http://share-it.herokuapp.com/hitchikes/search) -
Página que mostra as boleias pesquisadas (ou todas)

* `/hitchikes/show.html.erb` [/hitchikes/1](http://share-it.herokuapp.com/hitchikes/1) -
Página que mostra os detalhes de uma boleia.

* `/static_pages/home.html.erb` [/hitchikes/](http://share-it.herokuapp.com/) -
Página inicial

* `/static_pages/about.html.erb` [/about](http://share-it.herokuapp.com/about) -
Sobre nós

* `/static_pages/contact.html.erb` [/contact](http://share-it.herokuapp.com/contact) -
Contactos

* `/static_pages/terms.html.erb` [/terms](http://share-it.herokuapp.com/terms) - 
Termos

* `/layouts` é uma pasta especial. Já falo dela mais abaixo

* em `/reservations` não existe nada de interesse. provavelmente vai ser apagada.

* em `/devise` são páginas relacionadas com a autenticação. No nosso caso não vão ser muito precisas.

Bem, se repararem, o código HTML presente nos ficheiros `*.html.erb` não contém muitas tags necessárias,
como `<body>`, por exemplo, e `<html>`.

Isto pois, geralmente numa aplicação ou mesmo um site simples,
existe muita coisa que convém repetir em todas as páginas, como elementos de navegação, ou barras de topo, etc.


### Como é que todas as peças encaixam no final?


Em `layouts/application.html.erb` está a parte que é comum a todas as páginas.
Neste momento não tem muita coisa, apenas coisas no `<head>` que têm de estar lá 
e uma barra de navegação que contém o Login/Logout e Minhas boleias.

Da mesma forma, existe um CSS comum em toda a aplicação, está em `assets/stylesheets/application.css`

Então as páginas própriamente ditas, são *coladas* onde em `application.html.erb` aparece `<%= yield %>`.

Existem outras instruções como `<%= yield :head %>` .
Essas são substituídas pelo que está em `<% content_for :head %>` nas diversas páginas.
Esta instrução é útil no nosso caso, pois temos um CSS diferente para cada página.

`layouts/_footer.html.erb` também é um pedaço de código HTML que é colado
em todas as páginas e que representam aqueles links em baixo de navegação.

